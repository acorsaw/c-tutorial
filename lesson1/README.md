## The usual `Hello, World!` starting point

Using your editor lets make a new file. Because this is a
very simple program we are going to put it in all at once and then disect it
one word at a time.

```
#include <stdio.h>

main() {
    printf("Hello, World!\n");
}
```

Let's start with `#include`. This is a compiler directive that says include
information about this library. When a compiler is built there are some places
it will automaticaly look for files. In this case `/usr/include` is one of
them. The next thing is `<stdio.h>`.  If you open a terminal and
`ls /usr/include/stdio.h` you will see that it's an actual file. You can look
at the file but it won't make much sense yet. The important thing to know is
that all it does is include (thus the name of the directive) the content of
that file into your compiled output.

The next one is `main()`. The name `main` is a special function name. All C
programs start with the `main` function. The parenthesis `()` signify that it
doesn't take any arguments. Basically that means that you don't get to pass
anything to it. It just runs exactly what is there and you don't get to 
provide any additional information.

Next is the curly brace `{`. This tells the compiler that the start of the 
function body is coming up. There is a matching curly brace to close `}` the 
body so that the compiler knows when the function stops. If you are missing 
one of these or you have an extra you will end up with weird compile errors.

`printf` is the entire reason we did the __#include__ at the top of our program.
This is a C built in that will take a string (the stuff inside the quotes)
and print it to the console. It can do some pretty neat stuff like format
numbers and print special characters like a carriage return (the enter key).

After __printf__ there are parenthesis. We talked about parenthesis earlier
when discussing the __main__ function. They were empty before but in this
case there is something in it. We are passing the string contained in the
quotes to the __printf__ function. There has to be a matching parenthesis so
that the printf function knows where to stop. You will get similar weird errors
to the missing curly bracket if you have too few or too many parenthesis.

Finally inside the parenthesis is a string `"Hello, World!\n"`. This should
be familiar to anyone who has language experience but there is an extra bit
at the end. The `\n` is a character code that will put in a carriage return
when it runs so instead of this:

```
adam@adam-pie:~$ ./hello
Hello, World!adam@adam-pie:~$ 
```

You get a nice new line before the console prints your prompt again. I think we
can all agree this looks much nicer:

```
adam@adam-pie:~$ ./hello
Hello, World!
adam@adam-pie:~$
```

Go ahead and compile it using the `gcc` command that we used earlier to test
and see if we had everything installed correctly.

If we just run: `gcc hello.c` it will compile to the default output file of
`a.out`. It is executable so if you run it (`./a.out`) you will get the output
of:
```
adam@adam-pi:~/c-tutorial/lesson1$ ./a.out
Hello, World!
```

But, that's not really ideal. Having a program name of `a.out` is not helpful so
instead of renaming the file we can create a specific file name when we compile
using the `-o` flag like `gcc hello.c -o hello` so that now we can just do this:
```
adam@adam-pi:~/c-tutorial/lesson1$ ./hello 
Hello, World!
```

Great! But what about the nasty warning message when you compiled it? You
probably got something like this:
```
hello.c:3:1: warning: return type defaults to ‘int’ [-Wimplicit-int]
 main() {
 ^~~~

```

Warning messages are there for a reason so even though it worked we can fix it
so that it doesn't have a warning message any more. If we look closely at the
message we can get some important information about the warning. We know that it
happened in the `hello.c` file. That will be more important later when we have
more than one file but for now because we are only compiling one file it's not
that helpful.

The next thing we see is `:3:1: warning:` which tells us that it happened on the
third line at the first character and that it was a warning type message. If it
was an error message it would say that and the compilation would fail to produce
the output file.

Next we see the actual warning message `return type defaults to ‘int’ [-Wimplicit-int]`
. That's pretty helpful if you know about return types and functions. Each
function has a return type. In this case because it is a `main` function it has
an implicit return type of integer or `int` in C. We'll play with that a little
later but for now we just need to make our compiler a little less grumpy. To fix
this add `int` to the beginning of the `main` function so it looks like this:

```
int main() {
```

Now, compile it again and it should compile cleanly with no warning messages.
What we have done is say that the function `main` will return an integer when it
finishes.

After you run it again to make sure it still works run this command in your
shell:
```
echo $?
```

Did you get a zero as your output? In Unix land this is refered as a return code
or exit code. A zero indicates that it was successful. Any number (remember it
has to be an integer) besides zero would have indicated failure.

