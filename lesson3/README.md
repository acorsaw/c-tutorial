The C language standard library provides a lot of built in functionality for
application development. It is your responsibility to use these tools in
creative ways to solve problems. This is the fundamental job of any developer:
Create new tools using simple building blocks.

One of the most simple building blocks is the ability to read a character from
input. The `getchar()` function does exactly that. It has a counterpart
function `putchar(char)` that will print a character provided to it to the
output (usually a screen).

Since we have already written a `main()` and we also know about the while loop
this shouldn't come as a surprise to you:

```
#include <stdio.h>

int main() {
    int input = getchar();
    while(input != EOF) {
        putchar(input);
        input = getchar();
    }
}
```

This program is ready to run so copy it in and try it out. You should be able
to type in a line of text and see the same text in the console when you hit the
`Enter` key. There are a few things you will need to know about C if you read
other people's code. There are shorter ways to do this so let's step through
the code and figure out how it works.

In C the expression `input = getchar()` returns a value. It is the variable on
the left side. That will be a neat shortcut. We can just do something like:

```
while(input = getchar() != EOF) {
    ...
}
```

If you put that in your code and run it you don't get the results you're
looking for because the `!=` operator has precidence over the `=` operator so
a more accurate way to code what just happened is:

`while(input = (getchar() != EOF)) {`

That's why input is always 0 or 1. To fix this we can put in explicit ordering
so that the assignment happens before the comparison check:

```
while( (input = getchar()) != EOF ) {
    ...
}
```

Now the assignment to input is done and the resulting input is compared to
the EOF character.

Your new `main()` function looks like this:
```
int main() {
    int input;
    while((input = getchar()) != EOF) {
        putchar(input);
    }
}
```

If you were to describe this the narrative might go something like this:

1. Declare an `int` variable named `input`.
1. Get the input character using getchar() and if it is not equal to EOF...
1. Print the input character using putchar and check for more input.

While that's neat and all how useful is it really? Let's add some things we
might actually use.

We already know how to do some math on numbers so something like:
```
int characterCounter = 0;
characterCounter = characterCounter + 1;
```
Would seem pretty natural right? But this is C, we don't like typing any more
than necessary. So, there's a solution for that. The equivalant to that long
line above is simply:

```
int characterCounter = 0;
++characterCounter;
```

There are a couple other shortcut operators too like `--`, `+=`, and `-=`.
I'll show you an example of `+=` and leave the rest as an example to the
reader.

```
int characterCounter = 0;
characterCounter += 1;
```

Time to put the rubber to the road. Can you add a character counter to the
program so that when its done it prints out how many characters you put in?

It's ok I'll wait until you're ready to go again...

Great! Let's move on to the next thing. Maybe we need to know how many lines
are in it rather than just the total number of characters. We know (from 
previous lessons) that the `\n` character is a newline character. If we just
count the total number of new line characters we see we can also keep track
of the total number of lines as well. Let's introduce another control structure
called the `if` statement. This one is pretty natural:

```
if (value == condition) {
    do something here;
}
```

Two things of note here. The `value == condition` is any kind of true/false
statement but as C is so prone to do the False condition is 0 and any number
besides zero would be true.

The second thing is that we are using a double equal `==` and not just a single
equal. This is REALLY important in C and can cause some pretty bad errors
because if you mess it up it will result in an assignment rather than return a
boolean value.

Apparently Skynet was written in C by someone who didn't understand the
difference between assignment and checking for equality. Do you see what they
did wrong?

```
static bool isCrazyMurderingRobot = false;

void interactWithHumans(void){
    if(isCrazyMurderingRobot = true) {
        kill(humans);
    } else {
        beNiceTo(humans);
    }
}
```

Obligatory [Reddit post for your ammusement](https://i.redd.it/nf19afmd7kex.jpg)

Now that you have seen the power of the `if` statement you can check to see if
the character you want to count is the character that we're looking at by doing
something like this:

```
if (input == '\n') {
    // Do your thing here.
}
```

Alright, lesson time again. Modify your program to count the number of
characters and the number of newlines.

Bonus: print the number of characters so far after each newline like this:
```
This is a test.
This is a test. (16 characters and 1 lines)
This is another test.
This is another test. (38 characters and 2 lines)
```