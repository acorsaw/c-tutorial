# C Tutorial

Tutorial for teaching Jacob C

## Tutorial Layout
This will be a set of lessons that start with basic setup of a Windows machine
to do C development. Because I haven't worked with C in about 20 years I will be
learning from scratch as well as teaching. The general idea is that each lesson
will build off of the next lesson so the end result of lesson1 will be the
starting point of lesson 2. I don't really know where this is going to go so
hang tight and let's see where we end up.

## Lesson 0 - Environment Setup

All you really need to write and compile C code is a text editor and a
compiler. Let's start out with the compiler.

### Windows Setup

We're going to use [MinGW via a package from Win-Builds](http://win-builds.org/doku.php/download_and_installation_from_windows)
to install what we will need. After downloading the installer and running it you
will get options for system (choose Native Windows) and architecture (choose x86_64).
For the install directory I picked __C:\MinGW__. There is a __Process__ button
in the top right corner. When you click that it will start downloading all the
packages that are selected. By default that will be quite a few.

After it gets done add your new mingw/bin directory to your path so that you can
just type `gcc` or `g++` to compile your code.

### Raspberry Pi Setup

__Install Ubuntu Mate__

There are tons of ways to get Ubuntu Mate onto a flash card. If you go to the
[Ubuntu Mate for Raspberry Pi](https://ubuntu-mate.org/raspberry-pi/) page it
has tons of details on what you need to do. If you're using Windows to flash
a card I would recomend [Etcher](https://www.balena.io/etcher/) over some of
the recomendations on the Ubuntu Mate site.

__Update Ubuntu__
```
sudo apt-get update
sudo apt-get upgrade
```

__Install Chromium and Git__
```
sudo apt install chromium-browser
sudo apt install git
```

__Install Editor__

At some point you should work with `vi` but for now we are going to use
Visual Studio Code for Raspberry Pi. You can [download it here](https://github.com/stevedesmond-ca/vscode-arm/releases/latest)
and then install it by using `sudo apt install ./vscode-1.28.2.deb` where 
`vscode-1.28.2.deb` is the file downloaded by the latest release.


__Install Compiler__

You will need to install a C compiler. For this distribution all
you need to do is:

```
sudo apt install build-essential
```

When you're done you should get something like this:

```
adam@adam-pie:/var/log$ gcc --version
gcc (Ubuntu/Linaro 7.4.0-1ubuntu1~18.04) 7.4.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

### Lessons

__Lesson 1__: Hello World! With `main` and `printf`
__Lesson 2__: Variables, Math, While loops, and Constants
__Lesson 3__: Character input and output
__Lesson 4__: Working with arrays
__Lesosn 5__: Input multiple ways and command line parameters
