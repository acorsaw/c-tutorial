# Variables and Math

In lesson 1 we did install, setup, and disection of a small program. This time
we are going to use variables, a `while` loop, and use some more options to
format output using `printf`.

## More variables

To create a variable in C you give it a type and a name. For instance:

```
int age;
float weight;
```

We just created a variable named `age` with an integer type (whole numbers,
both positive and negative are allowed). We also created `weight` with a type
of float (it's more compliated but basically a positive or negative number
with a decimal point).

A value can be assigned to the variable using the equal sign like this:

```
age = 17;
weight = 155.8;
```

## Math with C

We can do math in C using standard computer math symbols (`+ - / *`). If we
wanted to figure out our grade for the semester we could do something like this:

```
int totalPoints = 1230;
int myPoints = 1114;

float myGrade = (myPoints / totalPoints) * 100;

printf("My grade is %.2f", myGrade);
```

There are some interesting things going on with this code. We both create and
assign a variable in a single line. We are using both `int` and `float` data
types.



Put that in your `hello.c` program, compile it like we did last time and see
what the output is.

Are you surprised by the output? Change the `totalPoints` and `myPoints` to be
`float` type instead of `int` type and then compile and run it. Is that closer
to what you expected? C is interesting in this respect. It did exactly what you
asked. It divided two integers and by default when you divide two integers you
get an integer. To get a float you need one (or both) of them to be a float
type. Once you switched them to float type it ended up with the right number.

We also added some extra things to our `printf` statement to format the decimal
place to two points. If we get a number like 93.4521458375456 it will print
out 93.45 because we use %.2f for the format. The `%.2f` means 'this is a palce
holder for a float variable printed to two numbers of precision'. You can give
it a total width of the number by adding a second number before the decimal
point to specify the total width of the number (including the decimal point)
like `printf("%7.2f", myGrade);` which will output this:
```
  90.57
```
Notice the leading space?

## Loops

Programs are really good at doing the same thing over and over again. If we
were to tell a grade schooler to count to 100 they would start at 1 and count
by one until they got to 100. A simple way to describe this would be something
like:

```
1 start at one
2 as long as the number is less than 100
3 say the number
4 add one to the number
5 go back to line 2
```

We can describe this using C as well using the `while` control structure like
this:
```
    int countingNumber = 1;

    while (countingNumber < 100) {
        printf("%d", countingNumber);
        countingNumber = countingNumber + 1;
    }
```

If we break this down it reads exactly the same way the pseudo code (that's a
term used in computer science to refer to code that describes what you want to
do but doesn't actually compile with anything) was written above. As long as
the condition inside the parenthesis results in a true statement the code inside
the curly braces will continue to run over and over again.

If you put this in your `hello.c` program and run it does it do what you
expected it to do? What do you think you will do to get it do work the way you
want it to? Here are two options:

```
while ( countingNumber < 101 ) {
    ...
}
```

and 

```
while ( countingNumber <= 100 ) {
    ...
}
```

For the exercise in this lesson print out a list of conversions between
Fahrenheit and Celsius for temperatures between -20F and 220F for every ten
degrees Fahrenheit.

Hint: The `\t` character is the special character for a Tab

Great! Now that you have convereted a bunch of values from Fahrenheit to
Celsius lets look at what some of your code might be. I bet you have a line
that looks something like:

```
int start = -20;

while(fahrenheit < 220) {
    fahrenheit = fahrenheit + 10;
    ...
}
```

In programming lingo those are magic numbers and they're not a good practice.
In order to get rid of them and make our code more managable we use constants
instead of magic numbers embeded in our code. Something like this is much more
maintainable and readable from a human perspective:

```
#define LOWER_LIMIT   -20
#define UPPER_LIMIT   220
#define STEP    10

while(fahrenheit < UPPER_LIMIT ) {
    fahrenheit = fahrenheit + STEP;
    ...
}
```

