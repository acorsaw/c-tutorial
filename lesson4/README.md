## Arrays and If/Else Additions

So far we have only dealt with a single variable either in the form of `int`,
`char`, `float`, and others. Sometimes it is nice to have a collection of
variables. C is no different than a lot of other languages in that it calls
these collections an `array`. The syntax is like this:

```
int grades[10];
```

Where `grades` is now an array of integers with a size of 10. We can refer to
an individual entry using an index of the array that starts with a zero. We can
visualize the array like this:

| 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
|---|---|---|---|---|---|---|---|---|---|
|90 | 81| 99| 84| 93| 90| 93| 84| 90| 96|

Since you know the starting number of the array index and you know how many are
in the array it should be pretty easy to find the maximum, minimum, average,
and total number of points (for this example we will assume that all assignments
were 100 points).

You can initialize small arrays with values like this:
```
int grades[10] = {90, 81, 99, 84, 93, 90, 93, 84, 90, 96};
```

Start with just looping over all 10 indexes (grades[0] through grades[9]) and
just print the value.

Once you have that working start by adding some incremental functionality.
The next thing we will add is keeping track of what the high score is. You
will need a variable to hold the max score and you will have to compare each
score with the max to see if it should be the new max.

In a similar fashion keep track of the minimum score and hold it until we're
all done.

You're on a roll now. Let's keep track of the total number of points (just add
each score to the sum of the previous scores).

Last thing, Compute the average of all the scores and print out the average
(with two decimal places of accuracy). If you need some help with the math and
printing on this one you might want to go back and review __Lesson 2__.

