So far I've been following [The C Programming Language](https://en.wikipedia.org/wiki/The_C_Programming_Language) 
book pretty closely because it is something that has stood the test of time.
But, I'd like to do something a little out of order so that we can write
something neat.

Lets start with the `getchar` function that we picked up in Lesson 3. The way
we used it was to take input from the command line when we typed it in. One
thing that we didn't do was to get input from somewhere else. That's actually
pretty easy using Unix. You have a couple of options:

```
cat README.md | ./wc
```

The `cat` command will concatenate two files together but is it often used to
just write a file to the screen (standard output). In this case we are using
`cat` on the README.md file and writing the output to standard output (the
screen). Then we use the pipe to take standard output and pass it to another
command. In this case our newly created Word Count program (wc).

Or you can take input directly from a file:

```
./wc < README.md
```

This is a little more straight forward. We are taking a file and redirecting
its contents to the program we wrote `wc`. The reason that we use `cat`
sometimes is because you can use it on its own and see the output. Try just
typing `cat README.md` and you'll see what I mean.

At the end of lesson 3 you had a working program that counted the number of
characters and the number of lines that you typed in. Since you were typing
them in it wasn't very hard to get a pretty solid number. What if you had a
paper due for class that you needed to know the exact number of words in it
but for some papers you needed to stay under a specific number of characters
and yet in others you needed to know how many lines you had. It sounds like
you would need three separate programs so that you only get the output you
wanted but there are ways to tell a program to change what it does.

We have already dealt with programs that take extra parameters like `ls` and
`mkdir`. If you would like to see the manual pages for `ls` or `mkdir` just
use the command `man ls` and `man mkdir` to see what I'm talking about.

In lesson 3 you ended up with something that looked a little like this:

```
#include <stdio.h>

int main() {

    int input;
    int numberOfCharacters;
    int numberOfLines;

    while((input = getchar()) != EOF) {
        putchar(input);
        if(input == '\n') {
            ++numberOfLines;
        }
        ++numberOfCharacters;
    }

    printf("Found %d characters and %d lines.\n", numberOfCharacters, numberOfLines);

}
```

We're going to modify the program so that we can do something like this:

`wc -l < README.md` to get the total nubmer of lines. `wc -w < README.md` to
get the total number of words. and `wc -c < README.md` to get the total number
of characters.

There is a built in way to read command line arguments in C called `getopt` that
is widely used in many programs. Its more complicated that what you've seen so
far so I'll provide a bit more of a hint that before but only because everyone
needs to look up examples to see how it works whenever they use it.

First import the Unix standard header file:
```
#include <unistd.h>
```

In most cases a program would take multiple arguments and you would want to
accept them in any order. For example, you would want `ls -l -a` to do the
exact same thing as `ls -a -l`. In order to do this it takes some code we
haven't seen yet so for the sake of this program we are just going to use an
`if` and `else if` statement because we are familiar with that. Let's start
with this:

```
#include <unistd.h>
#include <stdio.h>

int main(int argc, char *argv[]) {

    int opt;

    opt = getopt(argc, argv, "clw");
    if (opt == 'c') {
        printf("Count Characters\n");
    } else if (opt == 'l') {
        printf("Count Lines\n");
    } else if (opt == 'w') {
        printf("Count Words\n");
    } else if (opt == '?') {
        printf("That option was not one that I know about.\n");
    }

    return 0;

}
```

If you compile this program and run it you will be disappointed because it
doesn't do anything. It is because you didn't supply any command line
parameters. Run it again but this time give it a parameter like this:

```
gcc lesson5.c -o lesson5
./lesson5 -l
```

It should have printed "Count Lines" to the console if everything is correct.
If it didn't fix it up and we will move on. You can play with it a little bit
by giving it some other parameters like `-w`, `-c`, and `-v`. If you try using
`-v` does it do what you expected? If you provide a parameter that you did not
specify it will change it to a '?' so that you can trap it. In this case we
only specified the letters "c", "l", and "w" were valid when we called `getopt`
on this line:

```
opt = getopt(argc, argv, "clw");
```

Can you combine the program from lesson 3 with the code we just showed above
to read from standard input and then print the value you asked for? Start with
just the number of characters and then add in the number of lines. If you get
it right it should look something like this:

```
./lesson5 -l < README.md 
155
```

And for the characters:

```
./lesson5 -c < README.md
5214
```

